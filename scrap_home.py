import requests
from bs4 import BeautifulSoup

#constantes
URL = 'https://books.toscrape.com/'


def scrap_home():
    
    r = requests.get(URL)
    r.encoding = "utf-8"
    if r.ok :
        soup = BeautifulSoup(r.text, features="html.parser")
        return [URL + a["href"] for a in soup.select(".nav-list a")]
    else:
        raise Exception("")

if __name__ == "__main__" :
    for f in scrap_home():
        print(f)
        
