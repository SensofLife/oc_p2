import argparse
import sys
import csv
import re

import requests
from bs4 import BeautifulSoup

rating = {
            "One": "1",
            "Two": "2",
            "Three": "3",
            "Four": "4",
            "Five": "5"
}

def scrap_book(url):

    try: 
        r = requests.get(url)
        r.encoding = "utf8"
    except:
        raise Exception("url incorrect")
    if r.ok :
        soup = BeautifulSoup(r.text, features="html.parser")
        title = soup.select_one("h1").text

        upc = soup.select_one(".table-striped>tr:nth-child(1)>td").text
        
        price_et = soup.select_one(".table-striped>tr:nth-child(3)>td").text
        
        price_it = soup.select_one(".table-striped>tr:nth-child(4)>td").text
        
        availability = soup.select_one(".table-striped>tr:nth-child(6)>td").text
        availability_nbr = re.search("\d+", availability).group()
      
        description = soup.select_one("head>meta:nth-child(4)")["content"].strip()
     
        category = soup.select_one(".breadcrumb>li:nth-child(3)>a").text
      
        review_rating = soup.select_one(".star-rating")["class"][1]
        review_rating_nbr = rating[review_rating]
      
        image_url = soup.select_one(".item.active>img")["src"].replace("../..", "https://books.toscrape.com")
        
        return {
            "title": title,
            "upc": upc,
            "price_et": price_et,
            "price_it": price_it,
            "availability": availability_nbr,
            "description": description,
            "category": category,
            "review_rating": review_rating_nbr,
            "image_url": image_url

        }
    else:
        raise Exception("il y a une erreur")

if __name__ == "__main__" :
    parser = argparse.ArgumentParser()
    parser.add_argument("url" ,help="entrez l'url de la page du livre a scrapper")
    args = parser.parse_args()

    info_book =  scrap_book(args.url)
    
    print(info_book["title"])
    filename= info_book["title"] + ".csv"
    with open (filename, "w") as f:
        header = "title| upc| price_et| price_it| availability| description| category| review_rating| image_url\n"
        f.write(header)
        f.write(info_book["title"] + "|" + info_book["upc"]+ "|" + info_book["price_et"]+ "|" + info_book["price_it"]+ "|" + info_book["availability"]+ "|" + info_book["description"]+ "|" + info_book["category"]+ "|" + info_book["review_rating"]+ "|" + info_book["image_url"])
    