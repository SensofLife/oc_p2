import requests
from bs4 import BeautifulSoup
import argparse
import sys

def scrap_category(url):
    urls_pages_category = []
    while True:
        r = requests.get(url)
        r.encoding = "utf-8"
        if r.ok :
            soup = BeautifulSoup(r.text, features="html.parser")
            for a in soup.select(".image_container a"):
                urls_pages_category.append(a["href"].replace("../../..", "https://books.toscrape.com/catalogue"))
            #urls_pages_category.append([a["href"].replace("../../..", "https://books.toscrape.com/catalogue") for a in soup.select(".image_container a")])
            
            next_page_a = soup.select_one(".next>a")
            if not  next_page_a:
                break
            separate_url = url.split("/")
            separate_url.pop(-1)
            separate_url.append(next_page_a["href"])
            url_next_page = "/".join(separate_url)
            url = url_next_page
            
        else:
            raise Exception("il y a une erreur")
    return urls_pages_category

if __name__ == "__main__" :
    parser = argparse.ArgumentParser()
    parser.add_argument("url" ,help="entrez l'url de la page de la category a scrapper")
    args = parser.parse_args()

    
    print(len(scrap_category(args.url)))